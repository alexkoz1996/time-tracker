﻿using System;
using Repository.Models;

namespace Service.Requests
{
    public class TimeLogEditRequest : TimeLogRequest
    {
        /// <summary>
        /// Project id.
        /// </summary>
        public int ProjectRefId { get; set; }

        public void EditTimeLog(TimeLog timeLog)
        {
            if (string.IsNullOrEmpty(Comment))
            {
                throw new ArgumentNullException(nameof(Comment));
            }
            timeLog.Comment = Comment;

            if (ProjectRefId == 0)
            {
                throw new ArgumentException("ProjectRefId couldn`t be equal to 0");
            }
            timeLog.ProjectRefId = ProjectRefId;

            if (string.IsNullOrEmpty(TimeSpent))
            {
                throw new ArgumentException("TimeSpentInMinutes couldn`t be equal to 0");
            }
            timeLog.TimeSpentInMinutes = GetTimeSpentInMinutes(TimeSpent);

            timeLog.LogDateTime = LogDateTime;
        }
    }
}
