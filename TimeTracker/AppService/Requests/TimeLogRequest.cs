﻿using System;
using System.ComponentModel.DataAnnotations;
using Repository.DTO;
using Repository.Models;

namespace Service.Requests
{
    public class TimeLogRequest
    {
        public TimeLogRequest(TimeLog timeLog)
        {
            LogDateTime = timeLog.LogDateTime;
            TimeSpent = timeLog.GetFormattedTimeSpent();
            Comment = timeLog.Comment;
        }

        public TimeLogRequest(TimeLogDto dto)
        {
            LogDateTime = dto.LogDateTime;
            TimeSpent = dto.TimeSpent;
            Comment = dto.Comment;
        }

        public TimeLogRequest()
        {
        }

        /// <summary>
        /// Datetime of reporting.
        /// </summary>
        [Required]
        public DateTimeOffset LogDateTime { get; set; }

        /// <summary>
        /// Duration of working in minutes.
        /// </summary>
        [Required]
        public string TimeSpent { get; set; }

        /// <summary>
        /// Comment or description of executed work.
        /// </summary>
        [Required]
        public string Comment { get; set; }

        protected int GetTimeSpentInMinutes(string timeSpent)
        {
            var hPosition = timeSpent.IndexOf('h');
            var mPosition = timeSpent.IndexOf('m');

            int hours = 0;
            int minutes = 0;

            var areHoursCorrect = hPosition == -1 || Int32.TryParse(timeSpent.Substring(0, hPosition), out hours);
            var areMinutesCorrect =
                mPosition == -1 || Int32.TryParse(timeSpent.Substring(hPosition + 1, mPosition - 1 - hPosition), out minutes);

            if (!areMinutesCorrect || !areHoursCorrect)
            {
                throw new Exception("timeSpent string is nor correct");
            }

            var result = hours * 60 + minutes;
            return result;
        }
    }
}
