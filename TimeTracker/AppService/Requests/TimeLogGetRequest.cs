﻿using System.Globalization;
using Repository.DTO;
using Repository.Models;

namespace Service.Requests
{
    public class TimeLogGetRequest : TimeLogRequest
    {
        public TimeLogGetRequest(TimeLog timeLog) : base(timeLog)
        {
            ProjectTitle = timeLog.Project.Title;
            Id = timeLog.Id;
        }

        public TimeLogGetRequest(TimeLogDto dto) : base(dto)
        {
            ProjectTitle = dto.ProjectTitle;
            Id = dto.Id;
        }

        /// <summary>
        /// Id of time log.
        /// </summary>
        public int Id { get; set; }     

        /// <summary>
        /// Project title.
        /// </summary>
        public string ProjectTitle { get; set; }

        /// <summary>
        /// Log datetime for showing.
        /// </summary>
        public string FormattedLogDateTime => LogDateTime.ToString("D", new CultureInfo("en-US"));
    
    }
}
