﻿using Repository.DTO;

namespace Service.ServiceDto
{
    public class ProjectGetRequest
    {
        /// <summary>
        /// Id of project.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Title of project.
        /// </summary>
        public string Title { get; set; }

        public ProjectGetRequest(ProjectDto projectDto)
        {
            Id = projectDto.Id;
            Title = projectDto.Title;
        }
    }
}
