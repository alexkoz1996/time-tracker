﻿using System;
using System.ComponentModel.DataAnnotations;
using Repository.Models;

namespace Service.Requests
{
    public class TimeLogCreateRequest : TimeLogRequest
    {
        /// <summary>
        /// Project id.
        /// </summary>
        [Required]
        public int ProjectRefId { get; set; }

        public TimeLog CreateTimeLog(Project project)
        {
            var timeLog = new TimeLog();

            if (string.IsNullOrEmpty(Comment))
            {
                throw new ArgumentNullException(nameof(Comment));
            }
            timeLog.Comment = Comment;

            timeLog.Project = project;
            timeLog.ProjectRefId = ProjectRefId;

            if (string.IsNullOrEmpty(TimeSpent))
            {
                throw new ArgumentException("TimeSpent couldn`t be equal null or empty");
            }
            timeLog.TimeSpentInMinutes = GetTimeSpentInMinutes(TimeSpent);

            timeLog.LogDateTime = LogDateTime;
            return timeLog;
        }
        
    }
}
