﻿using System.Collections.Generic;
using System.Linq;
using Repository.Interfaces;
using Service.Interfaces;
using Service.ServiceDto;

namespace Service.Implementations
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;

        public ProjectService(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        /// <summary>
        /// Get all projects.
        /// </summary>
        /// <returns></returns>
        public List<ProjectGetRequest> GetAll()
        {
            var requests = _projectRepository.GetAll().Select(p => new ProjectGetRequest(p)).ToList();
            return requests;
        }
    }
}
