﻿using System;
using System.Collections.Generic;
using System.Linq;
using Repository.Interfaces;
using Service.Interfaces;
using Service.Requests;

namespace Service.Implementations
{
    /// <summary>
    /// Timelog service.
    /// </summary>
    public class TimeLogService : ITimeLogService
    {
        private readonly ITimeLogRepository _timeLogRepository;
        private readonly IProjectRepository _projectRepository;

        #region Constructors

        public TimeLogService(ITimeLogRepository timeLogRepository, IProjectRepository projectRepository)
        {
            _timeLogRepository = timeLogRepository;
            _projectRepository = projectRepository;
        }

        #endregion

        #region - public Methods - 

        /// <summary>
        /// Get all time logs.
        /// </summary>
        /// <param name="pageSize">Page size.</param>
        /// <param name="page">Page number.</param>
        /// <returns></returns>
        public List<TimeLogGetRequest> GetAll(int pageSize, int page = 0)
        {
            var requests = _timeLogRepository.GetAll(pageSize, page)
                .Select(t => new TimeLogGetRequest(t)).ToList();

            return requests;
        }

        /// <summary>
        /// Get all time logs.
        /// </summary>
        /// <returns></returns>
        public List<TimeLogGetRequest> GetAll()
        {
            var requests = _timeLogRepository.GetAll()
                .Select(t => new TimeLogGetRequest(t)).ToList();

            return requests;
        }

        /// <summary>
        /// Get time log by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TimeLogGetRequest Get(int id)
        {
            var timeLog = _timeLogRepository.GetById(id);           
            var request = new TimeLogGetRequest(timeLog);

            return request;
        }

        /// <summary>
        /// Create new time log.
        /// </summary>
        /// <param name="request">Time log request.</param>
        /// <returns>Time log identifier.</returns>
        public TimeLogGetRequest Create(TimeLogCreateRequest request)
        {
            var project = _projectRepository.GetById(request.ProjectRefId);

            if (project == null)
            {
                throw new ArgumentNullException("Project is not found");
            }

            var timeLog = request.CreateTimeLog(project);
            
            _timeLogRepository.Create(timeLog);

            var getTimeLog = new TimeLogGetRequest(timeLog);
            return getTimeLog;
        }

        /// <summary>
        /// Update time log.
        /// </summary>
        /// <param name="request">Time log request.</param>
        /// <param name="id">Time log id.</param>
        public void Update(TimeLogEditRequest request, int id)
        {
            var timeLog = _timeLogRepository.GetById(id);

            if (timeLog == null)
            {
                throw new ArgumentNullException("Time log is not found");
            }

            request.EditTimeLog(timeLog);
            _timeLogRepository.Update(timeLog);
        }

        /// <summary>
        /// Delete time log dto by id.
        /// </summary>
        /// <param name="id">Time log id.</param>
        public void Delete(int id)
        {
            var timeLog = _timeLogRepository.GetById(id);

            if (timeLog == null)
            {
                throw new ArgumentNullException("Time log is not found");
            }

            _timeLogRepository.Delete(id);
        }

        #endregion

    }
}
