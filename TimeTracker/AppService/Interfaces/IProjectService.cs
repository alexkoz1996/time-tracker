﻿using System.Collections.Generic;
using Service.ServiceDto;

namespace Service.Interfaces
{
    public interface IProjectService
    {
        /// <summary>
        /// Get all projects.
        /// </summary>
        /// <returns></returns>
        List<ProjectGetRequest> GetAll();
    }
}
