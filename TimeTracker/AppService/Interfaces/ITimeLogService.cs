﻿using System.Collections.Generic;
using Service.Requests;
using Service.ServiceDto;

namespace Service.Interfaces
{
    /// <summary>
    /// TimeLog service interface.
    /// </summary>
    public interface ITimeLogService
    {
        /// <summary>
        /// Get all time logs.
        /// </summary>
        /// <param name="pageSize">Page size.</param>
        /// <param name="page">Page number.</param>
        /// <returns></returns>
        List<TimeLogGetRequest> GetAll(int pageSize, int page = 0);

        /// <summary>
        /// Get all time logs.
        /// </summary>
        /// <returns></returns>
        List<TimeLogGetRequest> GetAll();

        /// <summary>
        /// Get time log by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TimeLogGetRequest Get(int id);

        /// <summary>
        /// Create new time log.
        /// </summary>
        /// <param name="request">Time log request.</param>
        /// <returns>Time log identifier.</returns>
        TimeLogGetRequest Create(TimeLogCreateRequest request);

        /// <summary>
        /// Update time log.
        /// </summary>
        /// <param name="request">Time log request.</param>
        /// <param name="id">Time log id.</param>
        void Update(TimeLogEditRequest request, int id);

        /// <summary>
        /// Delete time log dto by id.
        /// </summary>
        /// <param name="id">Time log id.</param>
        void Delete(int id);
    }
}
