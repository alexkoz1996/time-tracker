﻿using System;

namespace Repository.DTO
{
    public class TimeLogDto
    {
        /// <summary>
        /// Id of time log.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Datetime of reporting.
        /// </summary>
        public DateTimeOffset LogDateTime { get; set; }

        /// <summary>
        /// Duration of working in minutes.
        /// </summary>
        public string TimeSpent { get; set; }

        /// <summary>
        /// Comment or description of executed work.
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Project title.
        /// </summary>
        public string ProjectTitle { get; set; }
    }
}
