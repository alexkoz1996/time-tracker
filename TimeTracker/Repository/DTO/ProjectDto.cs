﻿namespace Repository.DTO
{
    public class ProjectDto
    {
        /// <summary>
        /// Id of project.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Title of project.
        /// </summary>
        public string Title { get; set; }
    }
}
