﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
    
namespace Repository.Models
{
    /// <summary>
    /// Unit of work time logging.
    /// </summary>
    public class TimeLog : BaseEntity
    {
        /// <summary>
        /// Datetime of reporting.
        /// </summary>
        public DateTimeOffset LogDateTime { get; set; }

        /// <summary>
        /// Duration of working in minutes.
        /// </summary>
        public int TimeSpentInMinutes { get; set; }

        /// <summary>
        /// Comment or description of executed work.
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Project id.
        /// </summary>
        public int ProjectRefId { get; set; }

        /// <summary>
        /// Project.
        /// </summary>
        [ForeignKey(nameof(ProjectRefId))]
        public virtual Project Project { get; set; }

        public string GetFormattedTimeSpent()
        {
            var hours = Convert.ToInt32(TimeSpentInMinutes / 60);
            var minutes = TimeSpentInMinutes  - hours * 60;
            var result = hours == 0 ? $"{minutes}m" : $"{hours}h{minutes}m";
            return result;
        }
    }
}
