﻿namespace Repository.Models
{
    /// <summary>
    /// Base entity.
    /// </summary>
    public abstract class BaseEntity
    {
        /// <summary>
        /// Identifier.
        /// </summary>
        public virtual int Id { get; set; }
    }
}
