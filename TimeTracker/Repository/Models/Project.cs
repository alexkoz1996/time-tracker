﻿using System.Collections.Generic;

namespace Repository.Models
{
    /// <summary>
    /// Project.
    /// </summary>
    public class Project : BaseEntity
    {
        /// <summary>
        /// Title of project.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// TimeLogs of project.
        /// </summary>
        public virtual ICollection<TimeLog> TimeLogs { get; set; }
    }
}
