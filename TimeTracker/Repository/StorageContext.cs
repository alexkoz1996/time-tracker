﻿using Microsoft.EntityFrameworkCore;
using Repository.Models;

namespace Repository
{
    public class StorageContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<TimeLog> TimeLogs { get; set; }

        public StorageContext(DbContextOptions<StorageContext> options) : base(options)
        {            
        }
    }
}
