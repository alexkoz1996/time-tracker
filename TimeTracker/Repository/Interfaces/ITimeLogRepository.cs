﻿using System.Collections.Generic;
using Repository.DTO;
using Repository.Models;

namespace Repository.Interfaces
{
    /// <summary>
    /// Timelog interface.
    /// </summary>
    public interface ITimeLogRepository : IRepository<TimeLog>
    {
        /// <summary>
        /// Get all time logs by specified page.
        /// </summary>
        /// <param name="pageSize">Size of page.</param>
        /// <param name="page">Page number.</param>
        /// <returns></returns>
        List<TimeLogDto> GetAll(int pageSize, int page = 0);

        /// <summary>
        /// Get all time logs by specified page.
        /// </summary>
        /// <returns></returns>
        List<TimeLogDto> GetAll();
    }
}
