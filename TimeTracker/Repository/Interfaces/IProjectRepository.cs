﻿using System.Collections.Generic;
using Repository.DTO;
using Repository.Models;

namespace Repository.Interfaces
{
    /// <summary>
    /// Project interface.
    /// </summary>
    public interface IProjectRepository : IRepository<Project>
    {
        /// <summary>
        /// Get all projects.
        /// </summary>
        /// <returns></returns>
        List<ProjectDto> GetAll();
    }
}
