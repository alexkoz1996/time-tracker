﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Repository.DTO;
using Repository.Interfaces;
using Repository.Models;

namespace Repository.Implementations
{
    /// <summary>
    /// Project repository.
    /// </summary>
    public class ProjectRepository : BaseRepository<Project>, IProjectRepository
    {
        #region - Constructors -

        public ProjectRepository(StorageContext context) : base(context)
        {
        }

        #endregion 

        #region - public Methods -

        /// <summary>
        /// Get all projects.
        /// </summary>
        /// <returns></returns>
        public List<ProjectDto> GetAll()
        {
            var dtos = Context.Projects
                .Select(p => new ProjectDto
            {
                Id = p.Id,
                Title = p.Title
            }).ToList();

            return dtos;
        }

        #endregion 

    }
}
