﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using Repository.Models;

namespace Repository.Implementations
{
    /// <summary>
    /// Base repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseRepository <T> : IRepository<T> where T : BaseEntity
    {
        protected readonly StorageContext Context;

        protected BaseRepository(StorageContext context)
        {
            Context = context;
        }

        //*********************************************************************

        #region Implementation of IRepository<T>

        /// <summary>
        /// Get entity by id.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <returns>Entity.</returns>
        public T GetById(int id)
        {
            return Context.Set<T>().Find(id);
        }

        /// <summary>
        /// Create new entity.
        /// </summary>
        /// <param name="newEntity">New entity.</param>
        /// <returns>Created entity.</returns>
        public T Create(T newEntity)
        {
            Context.Set<T>().Add(newEntity);
            Context.SaveChanges();
            return newEntity;          
        }

        /// <summary>
        /// Update entity.
        /// </summary>
        /// <param name="entity">Updated entity.</param>
        public void Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
        }

        /// <summary>
        /// Delete entity.
        /// </summary>
        /// <param name="id">Id of deleted entity.</param>
        public void Delete(int id)
        {
            var dbSet = Context.Set<T>();
            var entity = dbSet.Find(id);

            if (entity == null)
                throw new KeyNotFoundException();

            dbSet.Remove(entity);
            Context.SaveChanges();
        }

        #endregion

    }
}
