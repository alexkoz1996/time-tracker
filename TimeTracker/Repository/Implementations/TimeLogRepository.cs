﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Repository.DTO;
using Repository.Interfaces;
using Repository.Models;

namespace Repository.Implementations
{
    public class TimeLogRepository : BaseRepository<TimeLog>, ITimeLogRepository
    {
        #region - Constructors -

        public TimeLogRepository(StorageContext context) : base(context)
        {           
        }

        #endregion

        #region - public Methods -

        /// <summary>
        /// Get all time logs by specified page.
        /// </summary>
        /// <param name="pageSize">Size of page.</param>
        /// <param name="page">Page number.</param>
        /// <returns></returns>
        public List<TimeLogDto> GetAll(int pageSize, int page = 0)
        {
            var dtos = Context.TimeLogs
                .Select(t => new TimeLogDto
                {
                    Id = t.Id,
                    ProjectTitle = t.Project.Title,
                    Comment = t.Comment,
                    LogDateTime = t.LogDateTime,
                    TimeSpent = t.GetFormattedTimeSpent()
                }).ToList();
            var count = Context.TimeLogs.Count();
            var beginIndex = (page - 1) * pageSize;
            var rangeLength = (beginIndex + pageSize > count) ? (count - beginIndex) : pageSize;
            dtos = count <= pageSize ? dtos : dtos.GetRange(beginIndex, rangeLength);

            return dtos;
        }

        /// <summary>
        /// Get all time logs by specified page.
        /// </summary>
        /// <returns></returns>
        public List<TimeLogDto> GetAll()
        {
            var dtos = Context.TimeLogs
                .Select(t => new TimeLogDto
                {
                    Id = t.Id,
                    ProjectTitle = t.Project.Title,
                    Comment = t.Comment,
                    LogDateTime = t.LogDateTime,
                    TimeSpent = t.GetFormattedTimeSpent()
                }).ToList();
            return dtos;
        }

        public new TimeLog GetById(int id)
        {
            return Context.TimeLogs.Include(t => t.Project).ToList().First();
        }

        #endregion

    }
}
