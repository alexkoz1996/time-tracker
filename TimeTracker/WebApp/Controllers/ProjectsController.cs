﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Service.Interfaces;
using WebApp.Models;


namespace WebApp.Controllers
{
    [Route("api/projects")]
    public class ProjectsController : Controller
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public OkObjectResult Get()
        {
            var models = _projectService.GetAll();
            return Ok(new ApiResponse(models));
        }      
    }
}
