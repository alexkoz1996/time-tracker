﻿using System;
using Microsoft.AspNetCore.Mvc;
using Service.Interfaces;
using Service.Requests;
using WebApp.Models;


namespace WebApp.Controllers
{
    [Route("api/timeLogs")]
    public class TimeLogsController : Controller
    {
        private readonly ITimeLogService _timeLogService;

        public TimeLogsController(ITimeLogService timeLogService)
        {
            _timeLogService = timeLogService;
        }

        [HttpGet]
        public OkObjectResult Get(int pageSize, int page)
        {
            var models = _timeLogService.GetAll(pageSize, page);
            return Ok(new ApiResponse(models));
        }

        [HttpGet]
        [Route("pagesCount")]
        public OkObjectResult GetPagesCount(int pageSize)
        {
            var modelsCount = _timeLogService.GetAll().Count;
            var pageCount = Math.Ceiling((decimal)modelsCount / pageSize);
            return Ok(new ApiResponse(pageCount));
        }

        [HttpGet("{id}")]
        public OkObjectResult Get(int id)
        {
            var model = _timeLogService.Get(id);
            return Ok(new ApiResponse(model));
        }

        [HttpPost]
        public OkObjectResult Post([FromBody]TimeLogCreateRequest request)
        {
            var model = _timeLogService.Create(request);
            return Ok(new ApiResponse(model));
        }

        [HttpDelete("{id}")]
        public OkObjectResult Delete(int id)
        {
            _timeLogService.Delete(id);
            return Ok(new ApiResponse());
        }
    }
}
