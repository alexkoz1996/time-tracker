﻿namespace WebApp.Models
{
    public class ApiResponse
    {
        /// <summary>
        /// Returned object.
        /// </summary>
        public object Response { get; set; }

        /// <summary>
        /// Response code.
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// Additional information.
        /// </summary>
        public string Message { get; set; }

        public ApiResponse(object response)
        {
            Response = response;
            Code = (int)Models.Code.NoError;
            Message = null;
        }

        public ApiResponse()
        {
            
        }
    }
}
