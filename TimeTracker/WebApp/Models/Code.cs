﻿namespace WebApp.Models
{
    public enum Code
    {
        BadRequest = 400,
        NoError = 200
    }
}
